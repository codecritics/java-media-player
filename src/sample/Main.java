package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.net.URI;

public class Main extends Application {
    /**
     * First we instanciate a player which takes a scene;
     * The we set that scene to the primary Stage
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        Player player = new Player("file:src/resources/static/videos/test-2.mp4");
        Scene scene = new Scene(player, 720, 480, Color.BLACK);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
