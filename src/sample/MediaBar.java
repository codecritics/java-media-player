package sample;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.media.MediaPlayer;

public class MediaBar extends HBox {
    Slider timeSlider = new Slider();
    Slider volumeSlider = new Slider();

    Button playButton = new Button("||");

    Label volumeLabel = new Label("Volume :");

    MediaPlayer player;

    public MediaBar(MediaPlayer mediaPlayer) {
        player = mediaPlayer;

        setAlignment(Pos.CENTER);
        setPadding(new Insets(5, 10, 5, 10));

        volumeSlider.setPrefHeight(70);
        volumeSlider.setPrefWidth(30);
        volumeSlider.setValue(100);


        HBox.setHgrow(timeSlider, Priority.ALWAYS);

        playButton.setPrefWidth(30);

        getChildren().add(playButton);
        getChildren().add(timeSlider);
        getChildren().add(volumeLabel);
        getChildren().add(volumeSlider);

    }

}
